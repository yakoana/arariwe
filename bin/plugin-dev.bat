REM *********************************************
REM Environment
REM *********************************************
SET VC_REDIST=C:\opt\vstudio\VC\redist\x86\Microsoft.VC120.CRT
SET PROJECT_HOME=C:\arariwe\awe-project\Arariwe
SET FIREBREATH_HOME=C:\arariwe\firebreath
SET TARGET_DIR=C:\arariwe
SET OPENSSL_HOME=C:\arariwe\openssl\bin

SET PATH=%PATH%;%OPENSSL_HOME%
REM SEE http://www.firebreath.org/display/documentation/WiX+Installer+Help
SET _test=%WIX:~-1,1%
IF NOT %_test% == \ (
    GOTO OK
)
SET WIX=%WIX:~0,-1%
:OK
SET WIX_ROOT_DIR=%WIX%
%ComSpec%