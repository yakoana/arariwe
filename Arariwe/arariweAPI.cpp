#include "JSObject.h"
#include "variant_list.h"
#include "DOM/Document.h"
#include "global/config.h"
#include "fbjson.h"
#include "json/json.h"

#include "arariweAPI.h"
#include "boost/make_shared.hpp"
#include "boost/lexical_cast.hpp"

ArariweAPI::ArariweAPI
(
	_IN_ ArariwePtr& plugin,
	_IN_  FB::BrowserHostPtr& host
) :	m_plugin(plugin), m_host(host)
{
	registerAttribute
	(
		SHA1_WITH_RSA_ATTR,
		AWE_SHA1_RSA_PKCS,
		true
	);
	registerAttribute
	(
		SHA256_WITH_RSA_ATTR,
		AWE_SHA256_RSA_PKCS,
		true
	);
	registerAttribute
	(
		SHA512_WITH_RSA_ATTR,
		AWE_SHA512_RSA_PKCS,
		true
	);
	registerAttribute
	(
		SIGN_SUCCESS_ATTR,
		SIGN_SUCCESS,
		true
	);
	registerAttribute
	(
		SIGN_FAIL_ATTR,
		SIGN_FAIL,
		true
	);
	registerAttribute
	(
		VERIFY_FAIL_ATTR,
		VERIFY_FAIL,
		true
	);
	registerAttribute
	(
		INVALID_ARG_ATTR,
		INVALID_ARG,
		true
	);
	registerAttribute
	(
		SIGN_REFUSED_ATTR,
		SIGN_REFUSED,
		true
	);
	registerProperty
	(
		PROPERTY_VERSION,
		make_property
		(
			this,
			&ArariweAPI::version
		)
	);
	registerMethod
	(
		METHOD_CHECK,
		make_method
		(
			this,
			&ArariweAPI::check
		)
	);
	registerMethod
	(
		METHOD_ENUM_CERTS,
		make_method
		(
			this,
			&ArariweAPI::enum_certificates
		)
	);
	registerMethod
	(
		METHOD_SUPPORTS,
		make_method
		(
			this,
			&ArariweAPI::supports
		)
	);
	registerMethod
	(
		METHOD_SELECT,
		make_method
		(
			this,
			&ArariweAPI::sign_selected
		)
	);
	m_crypt = AWEDevice::instance();
}

ArariweAPI::~ArariweAPI()
{
	if (m_crypt) delete m_crypt;
}

ArariwePtr ArariweAPI::get_plugin()
{
	ArariwePtr plugin(m_plugin.lock());
	if (!plugin) throw FB::script_error(AWE_INVALID_PLUGIN_JSERROR);
	return plugin;
}

std::string ArariweAPI::version()
{
    return FBSTRING_PLUGIN_VERSION;
}

void ArariweAPI::check()
{
	try
	{
		m_crypt->check();
	}
	catch (...)
	{
		// TODO: LOG FB::script_error
	}
}

FB::VariantList ArariweAPI::enum_certificates()
{
	FB::VariantList ret;
	try
	{
		std::vector<AWE_CERT_ATTRIBUTES> attrs;
		m_crypt->enum_certificates(attrs)
		for (size_t i = 0; i < attrs.size(); i++)
		{
			FB::VariantMap jCert;
			AWE_CERT_ATTRIBUTES cert = attrs.at(i);
			jCert[JSON_HANDLE] = cert.handle;
			jCert[JSON_SUBJECT] = cert.subject;
			jCert[JSON_ISSUER] = cert.issuer;
			jCert[JSON_SERIAL] = cert.serialNumber;
			Json::Value jValue = FB::variantToJsonValue(jCert);
			Json::StyledWriter writer;
			std::string retVal = writer.write(jValue);
			ret.push_back(retVal);
		}
	}
	catch (std::exception& e)
	{
		// TODO: LOG FB::script_error
	}
	catch (...)
	{
		// TODO: LOG FB::script_error
	}
	return ret;
}

bool ArariweAPI::supports
(
	_IN_ AWE_CERT_HANDLE handle,
	_IN_ AWE_MECHANISM alg
)
{
	bool ret = false;
	try
	{
		ret = m_crypt->supports(handle, alg);
	}
	catch (std::exception& e)
	{
		// TODO: LOG FB::script_error
	}
	return ret;
}

#define AWE_CONVERT_VARIANT(in, out, type)				\
{											\
	try										\
	{										\
		out = in.convert_cast<type>();				\
	}										\
	catch (FB::bad_variant_cast&)						\
	{										\
		FB::script_error(AWE_INVALID_METHOD_ARGS_JSERROR);	\
	}										\
}
#define AWE_CONVERT_PARAM(param, name, out, type)			\
{											\
	FB::variant value = NULL;						\
	if (param->HasProperty(name))						\
	{										\
		value = param->GetProperty(name);				\
		AWE_CONVERT_VARIANT(value, out, type);			\
	}										\
}
#define AWE_PARAM_TO_INT(param, name, out)				AWE_CONVERT_PARAM(param, name, out, int)
#define AWE_PARAM_TO_BOOL(param, name, out)				AWE_CONVERT_PARAM(param, name, out, bool)
#define AWE_PARAM_TO_STRING(param, name, out)				AWE_CONVERT_PARAM(param, name, out, std::string)

#define AWE_GET_SIGN_PARAMS(_prms, _handle, _out)					\
{													\
	if												\
	(												\
		!_prms->HasProperty(SIGN_MECHANISM_PARAM) ||				\
		(											\
			_prms->HasProperty(SIGNING_CERT_PARAM) &&				\
			_prms->HasProperty(SIGN_POLICY_PARAM) &&				\
			(										\
				!_prms->HasProperty(POLICY_ID_PARAM) ||			\
				!_prms->HasProperty(POLICY_HASH_ALG_PARAM) ||		\
				!_prms->HasProperty(POLICY_HASH_PARAM) ||			\
				!_prms->HasProperty(POLICY_QUALIFIER_PARAM)		\
			)										\
		)											\
	)	FB::script_error(AWE_INVALID_METHOD_ARGS_JSERROR);			\
	AWE_MECHANISM alg = AWE_CRYPTO_UNDEFINED;						\
	bool attach = false, addSignCert = false, addSigPolicy = false;		\
	std::string policyId, policyHashAlg, policyHash, policyQualifier;		\
	AWE_PARAM_TO_INT(_prms, SIGN_MECHANISM_PARAM, md);				\
	AWE_PARAM_TO_BOOL(_prms, ATTACH_PARAM, attach);					\
	AWE_PARAM_TO_BOOL(_prms, SIGNING_CERT_PARAM, addSignCert);			\
	AWE_PARAM_TO_BOOL(_prms, SIGN_POLICY_PARAM, addSigPolicy);			\
	AWE_PARAM_TO_STRING(_prms, POLICY_ID_PARAM, policyId);			\
	AWE_PARAM_TO_STRING(_prms, POLICY_HASH_ALG_PARAM, policyHashAlg);		\
	AWE_PARAM_TO_STRING(_prms, POLICY_HASH_PARAM, policyHash);			\
	AWE_PARAM_TO_STRING(_prms, POLICY_QUALIFIER_PARAM, policyQualifier);	\
	if												\
	(												\
		alg == AWE_CRYPTO_UNDEFINED ||						\
		(											\
			addSigPolicy &&								\
			(										\
				policyId.size() == 0 ||						\
				policyHashAlg.size() == 0 ||					\
				policyHash.size() == 0 ||					\
				policyQualifier.size() == 0					\
			)										\
		)											\
	)	FB::script_error(AWE_INVALID_METHOD_ARGS_JSERROR);			\
	_out.cert = _handle;									\
	_out.mime = mimeEncapsulate;								\
	_out._prms.mechanism = alg;								\
	_out._prms.attach = attach;								\
	_out._prms.signingCert = addSignCert;						\
	_out._prms.signaturePolicy = addSigPolicy;					\
	_out._prms.sigPolicyId.assign(policyId);						\
	_out._prms.sigPolicyHashAlgorithm.assign(policyHashAlg);			\
	_out._prms.sigPolicyHashValue.assign(policyHash);				\
	_out._prms.sigPolicyQualifier.assign(policyQualifier);			\
}

void _sign_callback
(
	FB::JSAPIPtr jsapi,
	bool accepted,
	AWE_CERTIFICATE cert,
	AWE_SIGN_PARAMS signParams,
	std::string& contents,
	FB::JSObjectPtr jsCallback
)
{
	boost::shared_ptr<ArariweAPI> api(FB::ptr_cast<ArariweAPI>(jsapi));
	api->signContents(accepted, cert, signParams, contents, jsCallback);
}

void ArariweAPI::sign_selected
(
	_IN_ AWE_CERT_HANDLE handle,
	_IN_ bool mimeEncapsulate,
	_IN_ FB::JSObjectPtr& signParams,
	_IN_ FB::JSObjectPtr& callback
)
{
	AWE_SELECT_CALLBACK_PARAMS params;

	AWE_GET_SIGN_PARAMS(signParams, handle, params);
	if
	(
		params.mime &&
		!params.signParams.attach
	)	FB::script_error(AWE_INVALID_METHOD_ARGS_JSERROR);
	params.callback = callback;
#ifdef OS_WIN
	AWEWinCManager mgr;
#else
#endif	// OS_WIN
	mgr.select(get_plugin()->getRootJSAPI(), params, _sign_callback);
}

void ArariweAPI::signContents
(
	bool accepted,
	AWE_CERTIFICATE cert,
	AWE_SIGN_PARAMS signParams,
	std::string& contents,
	FB::JSObjectPtr jsCallback
)
{
	int retVal = SIGN_REFUSAL;
	std::string ret;
	if (accepted)
	{
		try
		{
			m_crypt->sign(cert, signParams, contents, ret);
			retVal = SIGN_SUCCESS;
		}
		catch (std::exception& e)
		{
			// LOG
			retVal = SIGN_FAILED;
		}

	}
	jsCallback->InvokeAsync("", FB::variant_list_of(retVal)(ret));
}
