#include "arariweAPI.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <stdlib.h>

#ifdef OS_WIN
#include <windows.h>
#include <urlmon.h>
#include <atlconv.h>
#else
#endif	// OS_WIN


void AWEContentManager::get_file
(
	_IN_ std::string& filepath,
	_OUT_ std::string& content
)	_THROW_
{
	std::ifstream in;
	char buffer[4096];
	in.open(filepath.c_str(), std::ifstream::in | std::ifstream::binary);
	if (!in.good()) throw AWEException(AWE_IO_ERROR);
	do
	{
		in.read(buffer, sizeof(buffer));
		content.append(buffer, in.gcount());
	}	while (in.good() && !in.eof());
	bool fail = in.bad();
	in.close();
	if (fail) throw AWEException(AWE_IO_ERROR);
}

#ifdef OS_WIN

void AWEWinCManager::select
(
	_IN_ FB::JSAPIPtr jsapi,
	_IN_ AWE_SELECT_CALLBACK_PARAMS params,
	_IN_ AWESignCallback callback
)
{
	if (!is_running)
	{
		is_running = true;
		boost::thread dlg
		(
			boost::bind
			(
				&AWEWinCManager::_select,
				this,
				jsapi,
				params,
				callback
			)
		);
	}
}

void AWEWinCManager::_select
(
	_IN_ FB::JSAPIPtr jsapi,
	_IN_ AWE_SELECT_CALLBACK_PARAMS params,
	_IN_ AWESignCallback callback
)
{
	std::stringbuf encapContent;
	bool accepted = false;
	try
	{
		std::string filepath, filename;
		if ((accepted = select_file(filepath, filename)))
		{
			std::string envelope;
			if
			(
				params.mime
			)	get_MIME_header(filepath, filename, envelope);
			std::string content;
			get_file(filepath, content);
			std::ostream os(&encapContent);
			os << envelope.c_str() << content.c_str();
		}
	}
	catch (std::exception& e)
	{
		// TODO: LOG
		accepted = false;
	}
	is_running = false;
	callback
	(
		jsapi,
		accepted,
		params.cert,
		params.signParams,
		encapContent.str(),
		params.callback
	);
}

bool AWEWinCManager::select_file
(
	_OUT_ std::string& path,
	_OUT_ std::string& filename
)	_THROW_
{
	TCHAR *filter = L"Todos os arquivos (*.*)\0*.*\0";
	TCHAR lpstrFile[MAX_PATH];
	TCHAR lpstrFileTitle[MAX_PATH];
	TCHAR *title = L"Assinar Documento";
	DWORD flags = OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST;
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	lpstrFile[0] = '\0';
	ZeroMemory(lpstrFileTitle, MAX_PATH);
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.lpstrFilter = filter;
	ofn.lpstrFile = lpstrFile;
	ofn.nMaxFile = MAX_PATH;
	ofn.lpstrFileTitle = lpstrFileTitle;
	ofn.nMaxFileTitle = MAX_PATH;
	ofn.lpstrTitle = title;
	ofn.Flags = flags;
	if (!GetOpenFileName(&ofn))
	{
		DWORD cderr = CommDlgExtendedError();
		if (cderr)
		{
			std::stringstream os;
			os << AWE_WIN_API_ERROR << cderr;
			throw AWEException(os.str());
		}
		else return false;
	}
	USES_CONVERSION;
	path.assign(T2A(lpstrFile));
	filename.assign(T2A(lpstrFileTitle));
	return true;
}

void AWEWinCManager::get_MIME_header
(
	_IN_ std::string& filepath,
	_IN_ std::string& filename,
	_OUT_ std::string& envelope
)
{
	USES_CONVERSION;
	std::string mimetype;
	LPWSTR ppwzMimeOut = NULL;
	if
	(
		FAILED
		(
			FindMimeFromData
			(
				NULL,
				A2T(filepath.c_str()),
				NULL,
				0,
				NULL,
				FMFD_URLASFILENAME,
				&ppwzMimeOut,
				0
			)
		)
	)
	{
		std::ifstream is;
		is.open
		(
			filepath.c_str(),
			std::ifstream::in | std::ifstream::binary
		);
		if (is.good())
		{
			char buf[256];
			size_t i;
			is.read(buf, 256);
			i = (size_t) is.gcount();
			is.close();
			if
			(
				FAILED
				(
					FindMimeFromData
					(
						NULL,
						NULL,
						buf,
						i,
						NULL,
						FMFD_DEFAULT,
						&ppwzMimeOut,
						0
					)
				)
			)	mimetype.assign("application/octet-stream");
		}	else	mimetype.assign("application/octet-stream");
	}
	if (ppwzMimeOut)
	{
		mimetype.assign(T2A(ppwzMimeOut));
		CoTaskMemFree(ppwzMimeOut);
	}
	std::stringbuf buffer;
	std::ostream os(&buffer);
	os << "MIME-Version: 1.0\r\n";
	os << "Content-Type: " << mimetype << "; name=" << filename << "\r\n";
	os << "Content-Transfer-Encoding: BINARY\r\n";
	os << "\r\n";
	envelope.assign(buffer.str());
}

#else
#endif	// OS_WIN
