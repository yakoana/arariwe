#include "arariweAPI.h"
#include <stdlib.h>
#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <time.h>
#include <string.h>

#define AWE_CERT_MAP_GET(_key, _val)	\
{							\
	AWE_GET_MAP					\
	(						\
		AWE_HANDLE_MAP,			\
		_map,					\
		_key,					\
		_val,					\
		AWE_INVALID_HANDLE_ERROR	\
	);						\
}

AWEDevice* AWEDevice::instance()
{
#ifdef CP_API
	return new AWEWinDevice();
#else
#endif	// CP_API
}

void AWEDevice::map_put
(
	_IN_ AWE_CERT_HANDLE* key,
	_IN_ AWE_PCERTIFICATE value
)	_THROW_
{
	if (!_map) throw AWEException(AWE_HANDLE_MAP_ERROR);
	khiter_t k;
	int tries = 0;
	do
	{
		NHC_random((NH_PBYTE) key, sizeof(int));
		k = kh_get(AWE_HANDLE_MAP, _map, *key);
		tries++;

	} while (k != kh_end(_map) && tries < 3);
	if (tries >= 3) throw AWEException(AWE_GEN_HANDLE_ERROR);
	int t;
	k = kh_put(AWE_HANDLE_MAP, _map, *key, &t);
	if (!t)
	{
		kh_del(AWE_HANDLE_MAP, _map, k);
		throw AWEException(AWE_STORE_CERT_ERROR);
	}
	else kh_val(_map, k) = value;
}

void AWEDevice::toBase64
(
	_IN_ unsigned char *in,
	_IN_ size_t inSize,
	_OUT_ std::string& b64
)	_THROW_
{
	NH_BASE64_HANDLER_PTR handler;
	NH_UINT len;
	NH_PBYTE buf;

	NHARU_ASSERT(NH_encode_base64((NH_PBYTE) in, inSize, &handler, &len));
	if (!(buf = (NH_PBYTE) malloc(len + 1)))
	{
		NH_finish_base64(handler, NULL);
		throw AWEException(AWE_OUT_MEMORY_ERROR);
	}
	buf[len] = '\0';
	NH_finish_base64(handler, buf);
	b64.assign((char *) buf);
	free(buf);
}

size_t AWEDevice::fromBase64
(
	_IN_ std::string& in,
	_OUT_ unsigned char* bin
)	_THROW_
{
	NH_BASE64_HANDLER_PTR handler;
	NH_UINT len;

	NHARU_ASSERT
	(
		NH_decode_base64
		(
			(NH_STRING) in.data(),
			in.size(),
			&handler,
			&len
		)
	);
	NH_finish_base64(handler, bin);
	return len;
}

#define AWE_ADD_SIGNATURE(_cert, _cms, _prm, _data, _out)				\
{													\
	try												\
	{												\
		NHIX_CMS_PSIGNER_INFO info;							\
		NH_PUINT hOID, sOID;								\
		NH_UINT hOIDCount, sOIDCount;							\
		NHC_MECHANISM hMech;								\
		sOID = nhc_rsaEncryption_oid;							\
		sOIDCount = NHC_RSA_ENCRYPTION_OID_COUNT;					\
		switch (_prm.mechanism)								\
		{											\
		case AWE_SHA1_RSA_PKCS:								\
			hOID = nhc_sha1_oid;							\
			hOIDCount = NHC_SHA1_OID_COUNT;					\
			hMech = NHC_SHA_1;							\
			break;									\
		case AWE_SHA256_RSA_PKCS:							\
			hOID = nhc_sha256_oid;							\
			hOIDCount = NHC_SHA256_OID_COUNT;					\
			hMech = NHC_SHA256;							\
			break;									\
		case AWE_SHA512_RSA_PKCS:							\
			hOIDCount = NHC_SHA512_OID_COUNT;					\
			hOID = nhc_sha512_oid;							\
			hMech = NHC_SHA512;							\
			break;									\
		default: throw AWEException(AWE_INVALID_ALG_ERROR);			\
		}											\
		NHARU_ASSERT									\
		(											\
			NHIX_cms_sd_add_digest_algorithm					\
			(										\
				_cms,									\
				hOID,									\
				hOIDCount								\
			)										\
		);											\
		add_chain(_cert, _cms);								\
		NHARU_ASSERT									\
		(											\
			NHIX_cms_sd_add_signer_info						\
			(										\
				_cms,									\
				&info									\
			)										\
		);											\
		put_sid(_cert, info);								\
		NHARU_ASSERT									\
		(											\
			NHIX_cms_sd_si_put_digest_algorithm					\
			(										\
				info,									\
				hOID,									\
				hOIDCount								\
			)										\
		);											\
		NHARU_ASSERT									\
		(											\
			NHIX_cms_sd_si_add_content_type					\
			(										\
				info,									\
				cms_data_ct_oid,							\
				PKIX_CMS_DATACT_OID_COUNT					\
			)										\
		);											\
		NH_UINT hSize;									\
		NH_PBYTE hHash;									\
		NHARU_ASSERT									\
		(											\
			NHC_digest									\
			(										\
				hMech,								\
				(NH_PBYTE) _data.data(),					\
				_data.size(),							\
				NULL,									\
				&hSize								\
			)										\
		);											\
		AWE_ASSERT_MEM									\
		(											\
			hHash = (NH_PBYTE) NHP_get_chunk					\
			(										\
				_container,								\
				hSize									\
			)										\
		);											\
		NHARU_ASSERT									\
		(											\
			NHC_digest									\
			(										\
				hMech,								\
				(NH_PBYTE) _data.data(),					\
				_data.size(),							\
				hHash,								\
				&hSize								\
			)										\
		);											\
		NHARU_ASSERT									\
		(											\
			NHIX_cms_sd_si_add_message_digest					\
			(										\
				info,									\
				hHash,								\
				hSize									\
			)										\
		);											\
		time_t now = time(NULL);							\
		NHARU_ASSERT									\
		(											\
			NHIX_cms_sd_si_add_signing_time					\
			(										\
				info,									\
				gmtime(&now)							\
			)										\
		);											\
		if											\
		(											\
			_prm.signingCert								\
		)	signing_cert(_cert, _prm.mechanism, info);			\
		if (_prm.signaturePolicy)							\
		{											\
			boost::char_separator<char> sep(".");				\
			std::vector<NH_UINT> oid;						\
			boost::tokenizer<boost::char_separator<char>> pIdTokens	\
			(										\
				_prm.sigPolicyId,							\
				sep									\
			);										\
			BOOST_FOREACH								\
			(										\
				const std::string& t,						\
				pIdTokens								\
			)	oid.push_back(std::stoul(t, nullptr, 10));		\
			NH_PUINT policyIdOID;							\
			NH_UINT policyIdOIDLen = oid.size();				\
			AWE_ASSERT_MEM								\
			(										\
				policyIdOID = (NH_PUINT) NHP_get_chunk			\
				(									\
					_container,							\
					policyIdOIDLen * sizeof(NH_UINT)			\
				)									\
			);										\
			for										\
			(										\
				size_t i = 0;							\
				i < oid.size();							\
				i++									\
			)	policyIdOID[i] = oid.at(i);					\
			oid.clear();								\
			boost::tokenizer<boost::char_separator<char>> pHashTokens	\
			(										\
				_prm.sigPolicyHashAlgorithm,					\
				sep									\
			);										\
			BOOST_FOREACH								\
			(										\
				const std::string& t,						\
				pHashTokens								\
			)	oid.push_back(std::stoul(t, nullptr, 10));		\
			NH_PUINT policyHashOID;							\
			NH_UINT policyHashOIDLen = oid.size();				\
			AWE_ASSERT_MEM								\
			(										\
				policyHashOID = (NH_PUINT) NHP_get_chunk			\
				(									\
					_container,							\
					policyHashOIDLen * sizeof(NH_UINT)			\
				)									\
			);										\
			for										\
			(										\
				size_t i = 0;							\
				i < oid.size();							\
				i++									\
			)	policyHashOID[i] = oid.at(i);					\
			NH_PBYTE policyHash;							\
			NH_UINT policyHashLen = fromBase64					\
			(										\
				_prm.sigPolicyHashValue,					\
				NULL									\
			);										\
			AWE_ASSERT_MEM								\
			(										\
				policyHash = (NH_PBYTE) NHP_get_chunk			\
				(									\
					_container,							\
					policyHashLen						\
				)									\
			);										\
			fromBase64(_prm.sigPolicyHashValue, policyHash);		\
			NHARU_ASSERT								\
			(										\
				NHIX_cms_sd_si_add_signature_policy				\
				(									\
					info,								\
					policyIdOID,						\
					policyIdOIDLen,						\
					policyHashOID,						\
					policyHashOIDLen,						\
					policyHash,							\
					policyHashLen,						\
					(NH_STRING) _prm.sigPolicyQualifier.data(),	\
					_prm.sigPolicyQualifier.size()			\
				)									\
			);										\
		}											\
		NHP_ASN1_PNODE clone;								\
		NH_UINT eSize;									\
		NH_PBYTE buffer, last_byte, next;						\
		AWE_ASSERT_MEM(clone = NHP_get_new_node(info->container));		\
		ASN_CLONE_NODE(info->signedAttrs, clone);					\
		if											\
		(											\
			!(eSize = NHP_encoded_size(clone))					\
		)	throw AWEException(AWE_ENCODING_ERROR);				\
		AWE_ASSERT_MEM									\
		(											\
			buffer = (NH_PBYTE) NHP_get_chunk					\
			(										\
				info->container,							\
				eSize									\
			)										\
		);											\
		NHARU_ASSERT(NHP_encode(clone, buffer));					\
		buffer[0] = NHP_ASN1_SET;							\
		NH_PBYTE signature;								\
		NH_UINT signatureLen;								\
		api_sign										\
		(											\
			_cert,									\
			_prm.mechanism,								\
			buffer,									\
			eSize,									\
			NULL,										\
			&signatureLen								\
		);											\
		AWE_ASSERT_MEM									\
		(											\
			signature = (NH_PBYTE) NHP_get_chunk				\
			(										\
				_container,								\
				signatureLen							\
			)										\
		);											\
		api_sign										\
		(											\
			_cert,									\
			_prm.mechanism,								\
			buffer,									\
			eSize,									\
			signature,									\
			&signatureLen								\
		);											\
		buffer[0] = 0xA0;									\
		last_byte = buffer + eSize - 1;						\
		NHARU_ASSERT									\
		(											\
			NHP_read_size								\
			(										\
				buffer + 1,								\
				last_byte,								\
				&info->signedAttrs->size,					\
				&info->signedAttrs->contents,					\
				&next									\
			)										\
		);											\
		info->signedAttrs->identifier = buffer;					\
		info->signedAttrs->child = NULL;						\
		NHARU_ASSERT									\
		(											\
			NHIX_cms_sd_si_put_signature_algorithm				\
			(										\
				info,									\
				sOID,									\
				sOIDCount								\
			)										\
		);											\
		NHARU_ASSERT									\
		(											\
			NHP_put_octet_string							\
			(										\
				info->container,							\
				info->signature,							\
				signature,								\
				signatureLen							\
			)										\
		);											\
	}												\
	catch (std::exception& e)								\
	{												\
		NHIX_release_cms(_cms);								\
		throw e;										\
	}												\
	NH_UINT encodedSize;									\
	NH_PBYTE encoded;										\
	try												\
	{												\
		if											\
		(											\
			!(encodedSize = NHIX_cms_get_encoded_size(_cms))		\
		)	throw AWEException(AWE_CMS_ENCODING_ERROR);			\
		AWE_ASSERT_MEM									\
		(											\
			encoded = (NH_PBYTE) NHP_get_chunk					\
			(										\
				_container,								\
				encodedSize								\
			)										\
		);											\
		NHARU_ASSERT(NHIX_cms_encode(_cms, encoded));				\
		NHIX_release_cms(_cms);								\
	}												\
	catch (std::exception& e)								\
	{												\
		NHIX_release_cms(_cms);								\
		throw e;										\
	}												\
	std::string b64;										\
	toBase64(encoded, encodedSize, b64);						\
	_out.assign("-----BEGIN PKCS7-----\r");						\
	_out.append(b64);										\
	_out.append("-----END PKCS7-----\r");						\
}


void AWEDevice::sign
(
	_IN_ AWE_CERT_HANDLE handle,
	_IN_ AWE_SIGN_PARAMS params,
	_IN_ std::string& contents,
	_OUT_ std::string& pem
)	_THROW_
{
	if (!_container) throw AWEException(AWE_CONTAINER_ERROR);
	int signers = 1;
	NHIX_PCMS hCMS;
	NHARU_ASSERT
	(
		NHIX_create_cms
		(
			NHIX_SIGNED_DATA_CTYPE,
			&signers,
			&hCMS
		)
	);
	try
	{
		NHARU_ASSERT(NHIX_cms_sd_put_version(hCMS, 0x01));
		NHARU_ASSERT
		(
			NHIX_cms_sd_put_content_type
			(
				hCMS,
				cms_data_ct_oid,
				PKIX_CMS_DATACT_OID_COUNT
			)
		);
		if (params.attach)
		{
			NHARU_ASSERT
			(
				NHIX_cms_sd_attach_content
				(
					hCMS,
					(NH_PBYTE) contents.data(),
					contents.size()
				)
			);
		}
	}
	catch (std::exception& e)
	{
		NHIX_release_cms(hCMS);
		throw e;
	}
	AWE_ADD_SIGNATURE(handle, hCMS, params, contents, pem);
}

void AWEDevice::cosign
(
	_IN_ std::string& cms,
	_IN_ AWE_CERT_HANDLE handle,
	_IN_ AWE_SIGN_PARAMS params,
	_IN_ bool verify,
	_OUT_ std::string& pem
)	_THROW_
{
	if (!_container) throw AWEException(AWE_CONTAINER_ERROR);
	NH_STRING b64buf;
	NH_UINT b64size;
	std::string b64;
	if
	(
		!NHIX_remove_pem_armour
		(
			(NH_STRING) cms.data(),
			cms.size(),
			&b64buf,
			&b64size
		)
	)	throw AWEException(AWE_PEM_FORMAT_ERROR);
	b64.assign(b64buf, b64size);
	unsigned char *buffer;
	size_t bufsize = fromBase64(b64, NULL);
	AWE_ASSERT_MEM
	(
		buffer = (unsigned char *) NHP_get_chunk
		(
			_container,
			bufsize
		)
	);
	fromBase64(b64, buffer);
	NHIX_PCMS hCMS;
	NHARU_ASSERT
	(
		NHIX_parse_cms
		(
			buffer,
			bufsize,
			NHIX_SIGNED_DATA_CTYPE,
			NULL,
			&hCMS
		)
	);
	std::string contents;
	try
	{
		NHP_ASN1_PNODE eContent;
		NH_PBYTE content;
		NHARU_ASSERT
		(
			NHIX_sd_parse_contents(hCMS)
		);
		eContent = ((NHIX_CMS_PSIGNED_DATA)hCMS->content)->eContent;
		if
		(
			!(
				eContent &&
				eContent->child &&
				eContent->child->next &&
				ASN_IS_PRESENT(eContent->child->next) &&
				eContent->child->next->child &&
				eContent->child->next->child->value &&
				eContent->child->next->child->valuelen
			)
		)	throw AWEException(AWE_UNDEFINED_MECHANISM_ERROR);
		AWE_ASSERT_MEM
		(
			content = (NH_PBYTE)NHP_get_chunk
			(
				_container,
				eContent->child->next->child->valuelen
			)
		);
		memcpy
		(
			content,
			eContent->child->next->child->value,
			eContent->child->next->child->valuelen
		);
		contents.assign((char *)content, eContent->child->next->child->valuelen);
		NHARU_ASSERT(NHIX_cms_parse_certificates(hCMS));
		if (verify)
		{
			NHP_ASN1_PNODE signerCerts;
			NH_UINT signerCertsCount;
			NHP_ASN1_PNODE pubkeyinfo;
			NH_BOOLEAN match;
			NHARU_ASSERT
			(
				NHIX_find_signer_certificate
				(
					hCMS,
					&signerCerts,
					&signerCertsCount
				)
			);
			NHARU_ASSERT
			(
				NHIX_get_pubkey_list
				(
					_container,
					&signerCerts,
					signerCertsCount,
					&pubkeyinfo
				)
			);
			NHARU_ASSERT
			(
				NHIX_verify_attached_cms_signed_data
				(
					hCMS,
					&pubkeyinfo,
					signerCertsCount,
					&match
				)
			);
			if (!match) throw AWEException(AWE_INVALID_CMS_SIG_ERROR);
		}
	}
	catch (std::exception& e)
	{
		NHIX_release_cms(hCMS);
		throw e;
	}
	AWE_ADD_SIGNATURE(handle, hCMS, params, contents, pem);
}

#ifdef CP_API
#include <atlconv.h>

#define WINAPI_GET_ERROR(_out)			\
{								\
	USES_CONVERSION;					\
	LPVOID lpMsgBuf;					\
	DWORD dw = GetLastError();			\
	FormatMessage					\
	(							\
		FORMAT_MESSAGE_ALLOCATE_BUFFER |	\
		FORMAT_MESSAGE_FROM_SYSTEM |		\
		FORMAT_MESSAGE_IGNORE_INSERTS,	\
		NULL,						\
		dw,						\
		0,						\
		(LPTSTR) &lpMsgBuf,			\
		0,						\
		NULL						\
	);							\
	_out << AWE_WIN_API_ERROR << dw << " "	\
	<< T2A((LPCTSTR) lpMsgBuf);			\
	LocalFree(lpMsgBuf);				\
}

#define WINAPI_ASSERT(_exp)				\
{								\
	if (!(_exp))					\
	{							\
		std::stringstream os;			\
		WINAPI_GET_ERROR(os);			\
		throw AWEException(os.str());		\
	}							\
}

#define WINAPI_ASSERT_OR_DIE(_exp, _die)		\
{								\
	if (!(_exp))					\
	{							\
		std::stringstream os;			\
		WINAPI_GET_ERROR(os);			\
		_die;						\
		throw AWEException(os.str());		\
	}							\
}

#define WINAPI_ASSIGN_NAME(_from, _to, _type)					\
{												\
	DWORD size;										\
	LPTSTR name;									\
	size = CertNameToStr(X509_ASN_ENCODING, &_from, _type, NULL, 0);	\
	if (!(name = (LPTSTR) malloc(size * sizeof(WCHAR))))			\
	{											\
		std::stringstream stream;						\
		stream << AWE_OUT_MEMORY_ERROR;					\
		throw AWEException(stream.str());					\
	}											\
	CertNameToStr(X509_ASN_ENCODING, &_from, _type, name, size);	\
	USES_CONVERSION;									\
	_to.assign(T2A(name));								\
	free(name);										\
}


AWEWinDevice::AWEWinDevice()
{
	NHARU_ASSERT(NHP_alloc_container(16, 16, 16384, 16384, &_container));
	_map = kh_init(AWE_HANDLE_MAP);
}

AWEWinDevice::~AWEWinDevice()
{
	release_certs();
	kh_destroy(AWE_HANDLE_MAP, _map);
	NHP_free_container(_container);
}

void AWEWinDevice::check()
{
	try
	{
		DWORD cbProvName;
		CryptGetDefaultProvider
		(
			PROV_RSA_FULL,
			NULL,
			CRYPT_USER_DEFAULT,
			NULL,
			&cbProvName
		);
	}
	catch (...)
	{
		throw AWEException(AWE_INVALID_PLUGIN_ERROR);
	}
}

void AWEWinDevice::release_certs()
{
	for (khiter_t k = kh_begin(_map); k != kh_end(_map); ++k)
	{
		if (kh_exist(_map, k))
		{
			AWE_PCERTIFICATE hCert = kh_val(_map, k);
			if
			(
				hCert->pfFree &&
				hCert->dwKeySpec != CERT_NCRYPT_KEY_SPEC
			)	CryptReleaseContext(hCert->hCryptProv, 0);
			CertFreeCertificateContext(hCert->pCtx);
			delete hCert;
		}
	}
}

void AWEWinDevice::enum_certificates
(
	_OUT_ std::vector<AWE_CERT_ATTRIBUTES>& list
)	_THROW_
{
	HCERTSTORE hStore;
	PCCERT_CONTEXT pCert = NULL;
	release_certs();
	WINAPI_ASSERT(hStore = CertOpenSystemStore(NULL, L"MY"));
	while ((pCert = CertEnumCertificatesInStore(hStore, pCert)))
	{
		HCRYPTPROV hProv = NULL;
		DWORD dwKeySpec = 0;
		BOOL pfFree = 0;
		if
		(
			CryptAcquireCertificatePrivateKey
			(
				pCert,
				0,
				NULL,
				&hProv,
				&dwKeySpec,
				&pfFree
			)
		)
		{
			AWE_CERT_ATTRIBUTES cert;
			AWE_PCERTIFICATE hCert = NULL;
			try
			{
				WINAPI_ASSIGN_NAME
				(
					pCert->pCertInfo->Subject,
					cert.nickname,
					CERT_SIMPLE_NAME_STR
				);
				WINAPI_ASSIGN_NAME
				(
					pCert->pCertInfo->Subject,
					cert.subject,
					CERT_X500_NAME_STR
				);
				WINAPI_ASSIGN_NAME
				(
					pCert->pCertInfo->Issuer,
					cert.issuer,
					CERT_X500_NAME_STR
				);
				toBase64
				(
					pCert->pCertInfo->SerialNumber.pbData,
					pCert->pCertInfo->SerialNumber.cbData,
					cert.serialNumber
				);
				hCert = new AWE_CERTIFICATE();
				hCert->pCtx = CertDuplicateCertificateContext(pCert);
				hCert->hCryptProv = hProv;
				hCert->dwKeySpec = dwKeySpec;
				hCert->pfFree = pfFree;
				map_put(&cert.handle, hCert);
				list.push_back(cert);
			}
			catch (std::exception& e)
			{
				if (hCert) delete hCert;
				if
				(
					pfFree &&
					dwKeySpec != CERT_NCRYPT_KEY_SPEC
				)	CryptReleaseContext(hProv, 0);
				CertCloseStore(hStore, 0);
				throw e;
			}
		}
	}
	CertCloseStore(hStore, 0);
}

bool AWEWinDevice::supports
(
	_IN_ AWE_CERT_HANDLE handle,
	_IN_ AWE_MECHANISM mechanism
)
{
	ALG_ID hash;
	switch (mechanism)
	{
	case AWE_SHA1_RSA_PKCS:
		hash = CALG_SHA1;
		break;
	case AWE_SHA256_RSA_PKCS:
		hash = CALG_SHA_256;
		break;
	case AWE_SHA512_RSA_PKCS:
		hash = CALG_SHA_512;
		break;
	default: return false;
	}
	BYTE pbData[sizeof(PROV_ENUMALGS)];
	DWORD pdwDataLen = sizeof(PROV_ENUMALGS);
	DWORD dwFlags = CRYPT_FIRST;
	AWE_PCERTIFICATE hCert;
	AWE_CERT_MAP_GET(handle, hCert);
	while
	(
		CryptGetProvParam
		(
			hCert->hCryptProv,
			PP_ENUMALGS,
			pbData,
			&pdwDataLen,
			dwFlags
		)
	)
	{
		PROV_ENUMALGS* alg = (PROV_ENUMALGS *) pbData;
		if
		(
			(GET_ALG_CLASS(alg->aiAlgid) == ALG_CLASS_HASH) &&
			(alg->aiAlgid == hash)
		)	return true;
		dwFlags = CRYPT_NEXT;
	}
	return false;
}

void AWEWinDevice::add_chain
(
	_IN_ AWE_CERT_HANDLE handle,
	_IN_ NHIX_PCMS hCMS
)	_THROW_
{
	AWE_PCERTIFICATE hCert;
	PCCERT_CHAIN_CONTEXT pChainCtx = NULL;
	CERT_CHAIN_PARA params;
	AWE_CERT_MAP_GET(handle, hCert);
	params.cbSize = sizeof(CERT_CHAIN_PARA);
	params.RequestedUsage.dwType = USAGE_MATCH_TYPE_AND;
	params.RequestedUsage.Usage.rgpszUsageIdentifier = NULL;
	params.RequestedUsage.Usage.cUsageIdentifier = 0;
	WINAPI_ASSERT
	(
		CertGetCertificateChain
		(
			NULL,
			hCert->pCtx,
			NULL,
			NULL,
			&params,
			0,
			NULL,
			&pChainCtx
		)
	);
	try
	{
		DWORD dwLen = pChainCtx->rgpChain[0]->cElement;
		NHP_ASN1_PNODE *chain = NULL;
		AWE_ASSERT_MEM
		(
			chain = (NHP_ASN1_PNODE *) NHP_get_chunk
			(
				_container,
				dwLen * sizeof(NHP_ASN1_PNODE)
			)
		);
		for (DWORD i = 0; i < dwLen; i++)
		{
			DWORD dwSize = pChainCtx->rgpChain[0]->rgpElement[i]->pCertContext->cbCertEncoded;
			NHP_ASN1_PNODE node;
			NH_PBYTE next;
			AWE_ASSERT_MEM(node = NHP_get_new_node(_container));
			node->knowledge = NHP_ASN1_SEQUENCE;
			AWE_ASSERT_MEM
			(
				node->identifier = (NH_PBYTE) NHP_get_chunk
				(
					_container,
					dwSize
				)
			);
			memcpy
			(
				node->identifier,
				pChainCtx->rgpChain[0]->rgpElement[i]->pCertContext->pbCertEncoded,
				dwSize
			);
			NHARU_ASSERT
			(
				NHP_read_size
				(
					node->identifier + 1,
					node->identifier + dwSize - 1,
					&node->size,
					&node->contents,
					&next
				)
			);
			chain[i] = node;
		}
		NHARU_ASSERT(NHIX_cms_sd_add_certificates(hCMS, chain, dwLen));
	}
	catch (std::exception& e)
	{
		CertFreeCertificateChain(pChainCtx);
		throw e;
	}
	CertFreeCertificateChain(pChainCtx);
}

void AWEWinDevice::put_sid
(
	_IN_ AWE_CERT_HANDLE handle,
	_IN_ NHIX_CMS_PSIGNER_INFO hInfo
)	_THROW_
{
	AWE_PCERTIFICATE hCert;
	NHIX_PCERTIFICATE nhCert;
	AWE_CERT_MAP_GET(handle, hCert);
	NHARU_ASSERT
	(
		NHIX_parse_certificate
		(
			hCert->pCtx->pbCertEncoded,
			hCert->pCtx->cbCertEncoded,
			&nhCert
		)
	);
	NHARU_ASSERT_OR_DIE
	(
		NHIX_cms_sd_si_put_sid_from_cert(hInfo, nhCert),
		NHIX_release_certificate(nhCert)
	);
	NHIX_release_certificate(nhCert);
}

void AWEWinDevice::signing_cert
(
	_IN_ AWE_CERT_HANDLE handle,
	_IN_ AWE_MECHANISM signingMechanism,
	_IN_ NHIX_CMS_PSIGNER_INFO hInfo
)	_THROW_
{
	NHC_MECHANISM hHash;
	switch (signingMechanism)
	{
	case AWE_SHA1_RSA_PKCS:
		hHash = NHC_SHA_1;
		break;
	case AWE_SHA256_RSA_PKCS:
		hHash = NHC_SHA256;
		break;
	case AWE_SHA512_RSA_PKCS:
		hHash = NHC_SHA512;
		break;
	default: throw AWEException(AWE_INVALID_ALG_ERROR);
	}
	NH_UINT hashSize;
	NH_PBYTE hash;
	AWE_PCERTIFICATE hCert;
	AWE_CERT_MAP_GET(handle, hCert);
	NHARU_ASSERT
	(
		NHC_digest
		(
			hHash,
			hCert->pCtx->pbCertEncoded,
			hCert->pCtx->cbCertEncoded,
			NULL,
			&hashSize
		)
	);
	AWE_ASSERT_MEM
	(
		hash = (NH_PBYTE) NHP_get_chunk
		(
			_container,
			hashSize
		)
	);
	NHARU_ASSERT
	(
		NHC_digest
		(
			hHash,
			hCert->pCtx->pbCertEncoded,
			hCert->pCtx->cbCertEncoded,
			hash,
			&hashSize
		)
	);
	NHARU_ASSERT
	(
		NHIX_cms_sd_si_add_signing_cert
		(
			hInfo,
			hHash,
			hash,
			hashSize
		)
	);
}

void AWEWinDevice::api_sign
(
	_IN_ AWE_CERT_HANDLE handle,
	_IN_ AWE_MECHANISM signingMechanism,
	_IN_ NH_PBYTE contents,
	_IN_ NH_UINT contentsLen,
	_OUT_ NH_PBYTE signature,
	_OUT_ NH_PUINT signatureLen
)	_THROW_
{
	ALG_ID hash;
	switch (signingMechanism)
	{
	case AWE_SHA1_RSA_PKCS:
		hash = CALG_SHA1;
		break;
	case AWE_SHA256_RSA_PKCS:
		hash = CALG_SHA_256;
		break;
	case AWE_SHA512_RSA_PKCS:
		hash = CALG_SHA_512;
		break;
	default: throw AWEException(AWE_INVALID_ALG_ERROR);
	}
	AWE_PCERTIFICATE hCert;
	HCRYPTHASH hHash = NULL;
	AWE_CERT_MAP_GET(handle, hCert);
	WINAPI_ASSERT
	(
		CryptCreateHash
		(
			hCert->hCryptProv,
			hash,
			0,
			0,
			&hHash
		)
	);
	try
	{
		if (signature)
		{
			WINAPI_ASSERT
			(
				CryptHashData
				(
					hHash,
					contents,
					contentsLen,
					0
				)
			);
		}
		WINAPI_ASSERT
		(
			CryptSignHash
			(
				hHash,
				AT_SIGNATURE,
				NULL,
				0,
				signature,
				(DWORD *) signatureLen
			)
		);
		CryptDestroyHash(hHash);
		if (signature) NH_swap(signature, *signatureLen);
	}
	catch (std::exception& e)
	{
		CryptDestroyHash(hHash);
		throw e;
	}
}

#else
#endif	// CP_API
