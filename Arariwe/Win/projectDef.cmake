#/**********************************************************\ 
# Auto-generated Windows project definition file for the
# Arariwe project
#\**********************************************************/

# Windows template platform definition CMake file
# Included from ../CMakeLists.txt

# remember that the current source dir is the project root; this file is in Win/
file (GLOB PLATFORM RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}
    Win/[^.]*.cpp
    Win/[^.]*.h
    Win/[^.]*.cmake
    )

# use this to add preprocessor definitions
add_definitions(
    /D "_ATL_STATIC_REGISTRY"
)

SOURCE_GROUP(Win FILES ${PLATFORM})

set (SOURCES
    ${SOURCES}
    ${PLATFORM}
    )

add_windows_plugin(${PROJECT_NAME} SOURCES)

# This is an example of how to add a build step to sign the plugin DLL before
# the WiX installer builds.  The first filename (certificate.pfx) should be
# the path to your pfx file.  If it requires a passphrase, the passphrase
# should be located inside the second file. If you don't need a passphrase
# then set the second filename to "".  If you don't want signtool to timestamp
# your DLL then make the last parameter "".
#
# Note that this will not attempt to sign if the certificate isn't there --
# that's so that you can have development machines without the cert and it'll
# still work. Your cert should only be on the build machine and shouldn't be in
# source control!
# -- uncomment lines below this to enable signing --
#firebreath_sign_plugin(${PROJECT_NAME}
#    "${CMAKE_CURRENT_SOURCE_DIR}/sign/certificate.pfx"
#    "${CMAKE_CURRENT_SOURCE_DIR}/sign/passphrase.txt"
#    "http://timestamp.verisign.com/scripts/timestamp.dll")

# add library dependencies here; leave ${PLUGIN_INTERNAL_DEPS} there unless you know what you're doing!
target_link_libraries(${PROJECT_NAME}
    ${PLUGIN_INTERNAL_DEPS}
    )


if (IE_DISTRIBUTION)
	set(WIX_HEAT_FLAGS
		-gg                 # Generate GUIDs
		-srd                # Suppress Root Dir
		-cg PluginDLLGroup  # Set the Component group name
		-dr INSTALLDIR      # Set the directory ID to put the files in
	)
	add_wix_installer( ${PLUGIN_NAME}
		${CMAKE_CURRENT_SOURCE_DIR}/Win/WiX/ArariweInstaller.wxs
		PluginDLLGroup
		${FB_BIN_DIR}/${PLUGIN_NAME}/${CMAKE_CFG_INTDIR}/
		${FB_BIN_DIR}/${PLUGIN_NAME}/${CMAKE_CFG_INTDIR}/${FBSTRING_PluginFileName}.dll
		${PROJECT_NAME}
	)
endif (IE_DISTRIBUTION)
    
# This is an example of how to add a build step to sign the WiX installer
# -- uncomment lines below this to enable signing --
#firebreath_sign_file("${PLUGIN_NAME}_WiXInstall"
#    "${FB_BIN_DIR}/${PLUGIN_NAME}/${CMAKE_CFG_INTDIR}/${PLUGIN_NAME}.msi"
#    "${CMAKE_CURRENT_SOURCE_DIR}/sign/certificate.pfx"
#    "${CMAKE_CURRENT_SOURCE_DIR}/sign/passphrase.txt"
#    "http://timestamp.verisign.com/scripts/timestamp.dll")

# This is an example of how to create a cab
# -- uncomment lines below this to enable signing --
#create_cab(${PLUGIN_NAME}
#    ${CMAKE_CURRENT_SOURCE_DIR}/Win/Wix/Arariwe.ddf
#    ${CMAKE_CURRENT_SOURCE_DIR}/Win/Wix/Arariwe.inf
#    ${FB_BIN_DIR}/${PLUGIN_NAME}/${CMAKE_CFG_INTDIR}/
#    ${PROJECT_NAME}_WiXInstallExe
#    )

FILE(TO_CMAKE_PATH "$ENV{WindowsSDK_IncludePath}" _TEMP)
ADD_HEADER_DIRECTORIES(INCLUDE_LIST "${_TEMP}/*.h")
find_library(_MSIE_LIB
	NAMES
		urlmon
	PATHS
		"$ENV{WindowsSDK_LibraryPath_x86}"
	)
list(
	APPEND LIB_LIST
	${_MSIE_LIB}
)
if (CRYPTOAPI_IMPL)
	find_library(_CRYPT_LIB
		NAMES
			crypt32
		PATHS
			"$ENV{WindowsSDK_LibraryPath_x86}"
		)
	list(
		APPEND LIB_LIST
		${_CRYPT_LIB}
	)
endif (CRYPTOAPI_IMPL)
set(OPENSSL_LIB	"libeay32MT")
add_definitions(-DOS_WIN)
set(
	CMAKE_SHARED_LINKER_FLAGS_DEBUG
	"${CMAKE_SHARED_LINKER_FLAGS_DEBUG} /NODEFAULTLIB:\"MSVCRT\" "
)
