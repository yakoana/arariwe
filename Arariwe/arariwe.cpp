#include "arariweAPI.h"
#include "arariwe.h"

///////////////////////////////////////////////////////////////////////////////
/// @fn Arariwe::StaticInitialize()
///
/// @brief  Called from PluginFactory::globalPluginInitialize()
///
/// @see FB::FactoryBase::globalPluginInitialize
///////////////////////////////////////////////////////////////////////////////
void Arariwe::StaticInitialize()
{
	// Place one-time initialization stuff here; As of FireBreath 1.4 this should only
	// be called once per process
	NHC_WEAK_SEED seed;
	NH_RV rv;

	if
	(
		NH_HAS_SUCCEEDED
		(
			rv = NHC_get_noise
			(
				(NH_PBYTE) seed,
				sizeof(NHC_WEAK_SEED)
			)
		)
	)	NHC_seed_random
		(
			(NH_PBYTE) seed,
			sizeof(NHC_WEAK_SEED)
		);
	else
	{
		// TODO: LOG
	}
}

///////////////////////////////////////////////////////////////////////////////
/// @fn Arariwe::StaticInitialize()
///
/// @brief  Called from PluginFactory::globalPluginDeinitialize()
///
/// @see FB::FactoryBase::globalPluginDeinitialize
///////////////////////////////////////////////////////////////////////////////
void Arariwe::StaticDeinitialize()
{
    // Place one-time deinitialization stuff here. As of FireBreath 1.4 this should
    // always be called just before the plugin library is unloaded
}

///////////////////////////////////////////////////////////////////////////////
/// @brief  Arariwe constructor.  Note that your API is not available
///         at this point, nor the window.  For best results wait to use
///         the JSAPI object until the onPluginReady method is called
///////////////////////////////////////////////////////////////////////////////
Arariwe::Arariwe()
{
	// TODO:
}

///////////////////////////////////////////////////////////////////////////////
/// @brief  Arariwe destructor.
///////////////////////////////////////////////////////////////////////////////
Arariwe::~Arariwe()
{
	releaseRootJSAPI();
	m_host->freeRetainedObjects();
}

void Arariwe::onPluginReady()
{
    // When this is called, the BrowserHost is attached, the JSAPI object is
    // created, and we are ready to interact with the page and such.  The
    // PluginWindow may or may not have already fire the AttachedEvent at
    // this point.
}

void Arariwe::shutdown()
{
    // This will be called when it is time for the plugin to shut down;
    // any threads or anything else that may hold a shared_ptr to this
    // object should be released here so that this object can be safely
    // destroyed. This is the last point that shared_from_this and weak_ptr
    // references to this object will be valid
}

///////////////////////////////////////////////////////////////////////////////
/// @brief  Creates an instance of the JSAPI object that provides your main
///         Javascript interface.
///
/// Note that m_host is your BrowserHost and shared_ptr returns a
/// FB::PluginCorePtr, which can be used to provide a
/// boost::weak_ptr<Arariwe> for your JSAPI class.
///
/// Be very careful where you hold a shared_ptr to your plugin class from,
/// as it could prevent your plugin class from getting destroyed properly.
///////////////////////////////////////////////////////////////////////////////
FB::JSAPIPtr Arariwe::createJSAPI()
{
	// m_host is the BrowserHost
	return boost::make_shared<ArariweAPI>
	(
		FB::ptr_cast<Arariwe>(shared_from_this()),
		m_host
	);
}
