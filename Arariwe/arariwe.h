#ifndef __ARARIWE_H__
#define __ARARIWE_H__

#include "PluginCore.h"


FB_FORWARD_PTR(Arariwe)
class Arariwe : public FB::PluginCore
{
public:
	static void StaticInitialize();
	static void StaticDeinitialize();

public:
	Arariwe();
	virtual ~Arariwe();

public:
	void onPluginReady();
	void shutdown();
	virtual FB::JSAPIPtr createJSAPI();
	virtual bool isWindowless() { return true; }

	BEGIN_PLUGIN_EVENT_MAP()
	END_PLUGIN_EVENT_MAP()

	/** BEGIN EVENTDEF -- DON'T CHANGE THIS LINE **/
	/** END EVENTDEF -- DON'T CHANGE THIS LINE **/
};


#endif	// __ARARIWE_H__
