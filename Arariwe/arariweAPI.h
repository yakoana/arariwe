#ifndef __ARARIWEAPI_H__
#define __ARARIWEAPI_H__


#ifdef _MSC_VER
#pragma warning(disable : 4290)
#pragma warning(disable : 4996)
#endif	// _MSC_VER

#include <boost/weak_ptr.hpp>
#include "JSAPIAuto.h"
#include "BrowserHost.h"

#include "arariwe.h"
#include <boost/function.hpp>

#include "nharu.h"
#ifdef __GNUC__
#undef  inline
#define inline	__inline__
#endif	// __GNUC__
#include "khash.h"
#define AWE_DECLARE_MAP(_name, _key, _value)	\
KHASH_INIT							\
(								\
	_name,						\
	_key,							\
	_value,						\
	1,							\
	kh_int_hash_func,					\
	kh_int_hash_equal					\
)

#include <string>
#include <vector>
#include <exception>
#include <sstream>


#ifdef CP_API
#include <windows.h>

typedef struct AWE_CERTIFICATE_ST
{
	PCCERT_CONTEXT	pCtx;
	HCRYPTPROV		hCryptProv;
	DWORD			dwKeySpec;
	BOOL			pfFree;

} AWE_CERTIFICATE, *AWE_PCERTIFICATE;

#else
#endif	// CP_API


typedef int			AWE_CERT_HANDLE;
typedef int			AWE_MECHANISM;

typedef struct AWE_CERT_ATTRIBUTES_ST
{
	AWE_CERT_HANDLE	handle;
	std::string		nickname;
	std::string		subject;
	std::string		issuer;
	std::string		serialNumber;

} AWE_CERT_ATTRIBUTES;

typedef struct AWE_SIGN_PARAMS_ST
{
	AWE_MECHANISM	mechanism;
	bool			attach;
	bool			signingCert;
	bool			signaturePolicy;
	std::string		sigPolicyId;
	std::string		sigPolicyHashAlgorithm;
	std::string		sigPolicyHashValue;
	std::string		sigPolicyQualifier;

} AWE_SIGN_PARAMS;

AWE_DECLARE_MAP
(
	AWE_HANDLE_MAP,
	AWE_CERT_HANDLE,
	AWE_PCERTIFICATE
)

typedef struct AWE_SELECT_CALLBACK_PARAMS_ST
{
	AWE_CERT_HANDLE		cert;
	bool				mime;
	AWE_SIGN_PARAMS		signParams;
	FB::JSObjectPtr		callback;

} AWE_SELECT_CALLBACK_PARAMS;

typedef boost::function<void (

	FB::JSAPIPtr jsapi,
	bool accepted,
	AWE_CERTIFICATE cert,
	AWE_SIGN_PARAMS signParams,
	std::string& contents,
	FB::JSObjectPtr jsCallback

)>	AWESignCallback;

#define _IN_		const
#define _OUT_
#define _NOTHROW_		throw()
#define _THROW_		throw (std::exception)

class AWEException : public std::exception
{
public:
	AWEException(std::string& msg) : _err(msg) {};
	AWEException(char* msg) : _err(msg) {};
	virtual ~AWEException() _NOTHROW_ {};
	virtual const char* what() _NOTHROW_ { return _err.c_str(); }
private:
	std::string _err;
};

class AWEDevice
{
public:

	virtual ~AWEDevice() {};
	static AWEDevice* instance();
	static void toBase64(_IN_ unsigned char*, _IN_ size_t, _OUT_ std::string& ) _THROW_;
	static size_t fromBase64(_IN_ std::string&, _OUT_ unsigned char*) _THROW_;
	void sign(_IN_ AWE_CERT_HANDLE, _IN_ AWE_SIGN_PARAMS, _IN_ std::string&, _OUT_ std::string&) _THROW_;
	void cosign(_IN_ std::string&, _IN_ AWE_CERT_HANDLE, _IN_ AWE_SIGN_PARAMS, _IN_ bool, _OUT_ std::string&) _THROW_;

public:

	virtual void check() = 0;
	virtual void enum_certificates(_OUT_ std::vector<AWE_CERT_ATTRIBUTES>& ) _THROW_ = 0;
	virtual bool supports(_IN_ AWE_CERT_HANDLE, _IN_ AWE_MECHANISM) _THROW_ = 0;

protected:

	AWEDevice() { _map = NULL; _container = NULL; };
	void map_put(_IN_ AWE_CERT_HANDLE*, _IN_ AWE_PCERTIFICATE) _THROW_;

protected:

	khash_t(AWE_HANDLE_MAP)* _map;
	NHP_ASN1_PCONTAINER _container;

private:

	virtual void add_chain(_IN_ AWE_CERT_HANDLE, _IN_ NHIX_PCMS) _THROW_ = 0;
	virtual void put_sid(_IN_ AWE_CERT_HANDLE, _IN_ NHIX_CMS_PSIGNER_INFO) _THROW_ = 0;
	virtual void signing_cert(_IN_ AWE_CERT_HANDLE, _IN_ AWE_MECHANISM, _IN_ NHIX_CMS_PSIGNER_INFO) _THROW_ = 0;
	virtual void api_sign(_IN_ AWE_CERT_HANDLE, _IN_ AWE_MECHANISM, _IN_ NH_PBYTE, _IN_ NH_UINT, _OUT_ NH_PBYTE, _OUT_ NH_PUINT) _THROW_ = 0;
};


#ifdef CP_API

class AWEWinDevice : public AWEDevice
{
public:

	AWEWinDevice();
	virtual ~AWEWinDevice();
	void check();
	void enum_certificates(_OUT_ std::vector<AWE_CERT_ATTRIBUTES>&) _THROW_;
	bool supports(_IN_ AWE_CERT_HANDLE, _IN_ AWE_MECHANISM)_THROW_;

private:

	void release_certs();

private:

	void add_chain(_IN_ AWE_CERT_HANDLE, _IN_ NHIX_PCMS) _THROW_;
	void put_sid(_IN_ AWE_CERT_HANDLE, _IN_ NHIX_CMS_PSIGNER_INFO) _THROW_;
	void signing_cert(_IN_ AWE_CERT_HANDLE, _IN_ AWE_MECHANISM, _IN_ NHIX_CMS_PSIGNER_INFO) _THROW_;
	void api_sign(_IN_ AWE_CERT_HANDLE, _IN_ AWE_MECHANISM, _IN_ NH_PBYTE, _IN_ NH_UINT, _OUT_ NH_PBYTE, _OUT_ NH_PUINT) _THROW_;
};

#else
#endif	// CP_API

class AWEContentManager
{
public:

	AWEContentManager() {};
	virtual ~AWEContentManager() {};

	virtual void select(_IN_ FB::JSAPIPtr, _IN_ AWE_SELECT_CALLBACK_PARAMS, _IN_ AWESignCallback) = 0;

protected:

	void get_file(_IN_ std::string&, _OUT_ std::string&) _THROW_;
	static bool is_running = false;
};

#ifdef OS_WIN

class AWEWinCManager : public AWEContentManager
{
public:

	AWEWinCManager() {};
	virtual ~AWEWinCManager() {};
	void select(_IN_ FB::JSAPIPtr, _IN_ AWE_SELECT_CALLBACK_PARAMS, _IN_ AWESignCallback);
	void _select(_IN_ FB::JSAPIPtr, _IN_ AWE_SELECT_CALLBACK_PARAMS, _IN_ AWESignCallback);

private:

	bool select_file(_OUT_ std::string&, _OUT_ std::string&) _THROW_;
	void get_MIME_header(_IN_ std::string&, _IN_ std::string&, _OUT_ std::string&);

};

#else
#endif	// OS_WIN

/****c* Arariwe/ArariweAPI
 *
 *  NAME
 *	ArariweAPI
 *
 *  PURPOSE
 *	Implementa a interface Javascript
 *
 *  TAGS
 *	application/x-arariwe
 *
 ******
 */
class ArariweAPI : public FB::JSAPIAuto
{
public:

	ArariweAPI(_IN_ ArariwePtr&, _IN_ FB::BrowserHostPtr&);
	virtual ~ArariweAPI();
	ArariwePtr get_plugin();

public:

	std::string version();
	void check();
	FB::VariantList enum_certificates();
	bool supports(_IN_ AWE_CERT_HANDLE, _IN_ AWE_MECHANISM);
	void sign_selected(_IN_ AWE_CERT_HANDLE, _IN_ bool, _IN_ FB::JSObjectPtr&, _IN_ FB::JSObjectPtr&);
	void signContents(bool accepted, AWE_CERTIFICATE cert, AWE_SIGN_PARAMS signParams, std::string& contents, FB::JSObjectPtr jsCallback);

private:

	ArariweWeakPtr m_plugin;
	FB::BrowserHostPtr m_host;
	AWEDevice *m_crypt;

};


// * * * * * * * * * * * * * * * * * * * * *
// Supported cryptographic mechanisms
// * * * * * * * * * * * * * * * * * * * * *
#define AWE_UNDEFINED_MECHANISM		 0
/****d* Arariwe/sha1WithRSAEncryption
 *
 *  NAME
 *	sha1WithRSAEncryption
 *
 *  DESCRIPTION
 *	Representa o algoritmo de assinatura digital 1.2.840.113549.1.1.5
 *
 ******
 */
#define SHA1_WITH_RSA_ATTR			"sha1WithRSAEncryption"
#define AWE_SHA1_RSA_PKCS			-1

/****d* Arariwe/sha256WithRSAEncryption
 *
 *  NAME
 *	sha256WithRSAEncryption
 *
 *  DESCRIPTION
 *	Representa o algoritmo de assinatura digital 1.2.840.113549.1.1.11
 *
 ******
 */
#define SHA256_WITH_RSA_ATTR			"sha256WithRSAEncryption"
#define AWE_SHA256_RSA_PKCS			-2

/****d* Arariwe/sha512WithRSAEncryption
 *
 *  NAME
 *	sha512WithRSAEncryption
 *
 *  DESCRIPTION
 *	Representa o algoritmo de assinatura digital 1.2.840.113549.1.1.13
 *
 ******
 */
#define SHA512_WITH_RSA_ATTR			"sha512WithRSAEncryption"
#define AWE_SHA512_RSA_PKCS			-3


// * * * * * * * * * * * * * * * * * * * * *
// Signature callback return code
// * * * * * * * * * * * * * * * * * * * * *
/****d* Arariwe/signSucceeded
 *
 *  NAME
 *	signSucceeded
 *
 *  DESCRIPTION
 *	Assinatura (ou co-assinatura) realizada com sucesso.
 *
 *  SEE ALSO
 *	Métodos de assinatura
 *
 ******
 */
#define SIGN_SUCCESS_ATTR			"signSucceeded"
#define SIGN_SUCCESS				1

/****d* Arariwe/signFailed
 *
 *  NAME
 *	signFailed
 *
 *  DESCRIPTION
 *	Falha interna na realização da assinatura (ou co-assinatura).
 *
 *  SEE ALSO
 *	Métodos de assinatura
 *
 ******
 */
#define SIGN_FAIL_ATTR				"signFailed"
#define SIGN_FAIL					2

/****d* Arariwe/verifyFailed
 *
 *  NAME
 *	verifyFailed
 *
 *  DESCRIPTION
 *	Numa co-assinatura, falha verficação criptográfica das assinaturas já existentes.
 *
 *  SEE ALSO
 *	Métodos de assinatura
 *
 ******
 */
#define VERIFY_FAIL_ATTR			"verifyFailed"
#define VERIFY_FAIL				3

/****d* Arariwe/invalidArgument
 *
 *  NAME
 *	invalidArgument
 *
 *  DESCRIPTION
 *	Chamada a um método de assinatura com argumentos inválidos.
 *
 *  SEE ALSO
 *	Métodos de assinatura
 *
 ******
 */
#define INVALID_ARG_ATTR			"invalidArgument"
#define INVALID_ARG				4

/****d* Arariwe/signRefused
 *
 *  NAME
 *	signRefused
 *
 *  DESCRIPTION
 *	O usuário recusou a operação de assinatura digital
 *
 *  SEE ALSO
 *	Métodos de assinatura
 *
 ******
 */
#define SIGN_REFUSED_ATTR			"signRefused"
#define SIGN_REFUSED				5

// * * * * * * * * * * * * * * * * * * * * *
// Plugin methods
// * * * * * * * * * * * * * * * * * * * * *
/****d* Arariwe/version
 *
 *  NAME
 *	version
 *
 *  DESCRIPTION
 *	Retorna a versão corrente do plugin
 *
 ******
 */
#define PROPERTY_VERSION			"version"

/****m* Arariwe/check
 *
 *  NAME
 *	check
 *
 *  USAGE
 *
 *  PURPOSE
 *	Verifica a instalação do sabor apropriado do plugin.
 *
 *  ERRORS
 *	Em caso de falha, dispara uma exceção Javascript.
 *
 ******
 */
#define METHOD_CHECK				"check"

/****m* Arariwe/enumerateCertificates
 *
 *  NAME
 *	enumerateCertificates
 *
 *  USAGE
 *
 *  PURPOSE
 *	Enumera os certificados instalados que possam ser utilizados para assinatura digital.
 *
 *  RESULT
 *	Retorna um array de strings JSON com os seguintes membros:
 *		Number Handle: identificador interno do certificado.
 *		String Subject: DN (Distinguished Name) do titular do certificado.
 *		String Issuer: DN do emissor do certificado.
 *		String SerialNumber: número serial do certificado, codificado em Base64. Note que o par Issuer/SerialNumber deve é suficiente para
 *		identificar inequivocamente um certificado emitido no escopo de uma PKI qualquer.
 *	A seguir um exemplo do valor de um membro do array de retorno:
 *	{
 *		"Handle" : -2004432863,
 *		"Issuer" : "CN=Common Name for All Cats End User CA,OU=PKI Ruler for All Cats,O=PKI Brazil,C=BR",
 *		"SerialNumber" : "CQ==",
 *		"Subject" : "CN=Meu primeiro certificado,OU=PKI Ruler for All Cats,O=PKI Brazil,C=BR"
 *	}
 *
 *  ERRORS
 *	Em caso de falha, dispara uma exceção Javascript.
 *
 ******
 */
#define METHOD_ENUM_CERTS			"enumerateCertificates"
#define JSON_HANDLE				"Handle"
#define JSON_SUBJECT				"Subject"
#define JSON_ISSUER				"Issuer"
#define JSON_SERIAL				"SerialNumber"

/****m* Arariwe/supports
 *
 *  NAME
 *	supports
 *
 *  USAGE
 *
 *  PURPOSE
 *	Determina se o certificado pode ser utilizado com o algoritmo especificado para assinatura.
 *
 *  INPUTS
 *	handle: o membro Handle do objeto retornado por enumerateCertificates().
 *	alg: uma das constantes criptográficas suportadas pelo plugin.
 *
 *  RESULT
 *	Retorna true se o mecanismo for suportado; caso contrário, false.
 *
 *  ERRORS
 *	Em caso de falha, dispara uma exceção Javascript.
 *
 ******
 */
#define METHOD_SUPPORTS				"supports"

/****m* Arariwe/selectAndSign
 *
 *  NAME
 *	selectAndSign
 *
 *  USAGE
 *
 *  PURPOSE
 *
 *
 *  INPUTS
 *
 *
 *  RESULT
 *
 *  NOTES
 *
 *  ERRORS
 *
 *  SEE ALSO
 *
 ******
 */
#define METHOD_SELECT				"selectAndSign"

/****c* Arariwe/AWE_SIGN_PARAMS
 *
 *  NAME
 *	AWE_SIGN_PARAMS
 *
 *  USAGE
 *
 *  PURPOSE
 *	Objeto Javascript utilizado para a passagem de parâmetros para os métodos de assinatura digital
 *
 *  ATTRIBUTES
 *	Number mechanism: constante do algoritmo criptográfico de assinatura digital a ser utilizado.
 *	Boolean attach: indicador de anexação ou não do conteúdo assinado.
 *	Boolean signingCert: indicador de utilização do padrão CAdES-BES.
 *	Boolean signaturePolicy: indicador de utilização do padrão CAdES-EPES. A utilização deste padrão requer que a propriedade
 *		signingCert esteja definida e contenha o valor true.
 *	String sigPolicyId: OID da política de assinatura a ser utilizada. Obrigatório na utilização do padrão CAdES-EPES.
 *	String sigPolicyHashAlgorithm: OID do algoritmo utilizado para calcular o hash da política de assinatura passada em
 *		sigPolicyId. Obrigatório na utilização do padrão CAdES-EPES.
 *	String sigPolicyHashValue: hash da política de assinatura passada em sigPolicyId codificado em Base64.
 *		Obrigatório na utilização do padrão CAdES-EPES.
 *	String sigPolicyQualifier: URL da política de assinatura utilizada. Obrigatório na utilização do padrão CAdES-EPES.
 *
 *  SEE ALSO
 *
 ******
 */
#define SIGN_MECHANISM_PARAM			"mechanism"
#define ATTACH_PARAM				"attach"
#define SIGNING_CERT_PARAM			"signingCert"
#define SIGN_POLICY_PARAM			"signaturePolicy"
#define POLICY_ID_PARAM				"sigPolicyId"
#define POLICY_HASH_ALG_PARAM			"sigPolicyHashAlgorithm"
#define POLICY_HASH_PARAM			"sigPolicyHashValue"
#define POLICY_QUALIFIER_PARAM		"sigPolicyQualifier"


// * * * * * * * * * * * * * * * * * * * * *
// Javascript error messages
// * * * * * * * * * * * * * * * * * * * * *
#define AWE_INVALID_PLUGIN_JSERROR		"The plugin is invalid"
#define AWE_INVALID_METHOD_ARGS_JSERROR	"Invalid method argments"


// * * * * * * * * * * * * * * * * * * * * *
// Exceptions error messages
// * * * * * * * * * * * * * * * * * * * * *
#define AWE_INVALID_PLUGIN_ERROR		"Unsuitable plugin flavour "
#define AWE_NHARU_ERROR_MESSAGE		"Nharu library has returned the following error code and message: "
#define AWE_INVALID_HANDLE_ERROR		"Invalid certificate handle"
#define AWE_OUT_MEMORY_ERROR			"Out of memory error"
#define AWE_INVALID_ALG_ERROR			"Unsupported signing mechanism"
#define AWE_WIN_API_ERROR			"Windows API has returned the following error code and message: "
#define AWE_GEN_HANDLE_ERROR			"Could not generate certificate handle"
#define AWE_STORE_CERT_ERROR			"Could not store certificate reference"
#define AWE_HANDLE_MAP_ERROR			"Certificate handles hash map has not been initialized"
#define AWE_CONTAINER_ERROR			"Memory container has not been initialized"
#define AWE_ENCODING_ERROR			"Unexpected CMS DER encoding"
#define AWE_CMS_ENCODING_ERROR		"Coult not encode CMS"
#define AWE_PEM_FORMAT_ERROR			"Only PEM encoded PKCS #7 are supported"
#define AWE_UNDEFINED_MECHANISM_ERROR	"Signed document must be attached"
#define AWE_INVALID_CMS_SIG_ERROR		"One of the CMS signatures does not match"
#define AWE_IO_ERROR				"File I/O error"


#define NHARU_ASSERT(exp)				\
{								\
	if (NH_HAS_FAILED((exp)))			\
	{							\
		std::stringstream stream;		\
		NH_RV rv;					\
		NH_STRING err;				\
		rv = NH_get_last_error(&err);		\
		stream << AWE_NHARU_ERROR_MESSAGE	\
		<< rv	<< err;				\
		throw AWEException(stream.str());	\
	}							\
}

#define NHARU_ASSERT_OR_DIE(exp, die)		\
{								\
	if (NH_HAS_FAILED((exp)))			\
	{							\
		std::stringstream stream;		\
		NH_RV rv;					\
		NH_STRING err;				\
		rv = NH_get_last_error(&err);		\
		stream << AWE_NHARU_ERROR_MESSAGE	\
		<< rv	<< " " << err;			\
		die;						\
		throw AWEException(stream.str());	\
	}							\
}


#define AWE_GET_MAP(_NAME, _MAP, _KEY, _VAL, _MSG)			\
{											\
	khiter_t k = kh_get(_NAME, _MAP, _KEY);				\
	if (k == kh_end(_MAP)) throw AWEException(_MSG);		\
	_VAL = kh_value(_MAP, k);						\
}

#define AWE_ASSERT_MEM(_exp)					if (!(_exp)) throw AWEException(AWE_OUT_MEMORY_ERROR);
#define AWE_ASSERT_MEM_OR_DIE(_exp, _die)			\
{									\
	if (!(_exp))						\
	{								\
		_die;							\
		throw AWEException(AWE_OUT_MEMORY_ERROR);	\
	}								\
}


#endif	// __ARARIWEAPI_H__

