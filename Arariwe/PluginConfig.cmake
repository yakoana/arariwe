#/**********************************************************\
#
# Auto-Generated Plugin Configuration file
# for Arariwe
#
#\**********************************************************/

set(PLUGIN_NAME					"Arariwe")
set(PLUGIN_PREFIX					"AWE")
set(COMPANY_NAME					"arariwe")

# ActiveX constants:
set(FBTYPELIB_NAME				ArariweLib)
set(FBTYPELIB_DESC				"Arariwe 1.0 Type Library")
set(IFBControl_DESC				"Arariwe Control Interface")
set(FBControl_DESC				"Arariwe Control Class")
set(IFBComJavascriptObject_DESC		"Arariwe IComJavascriptObject Interface")
set(FBComJavascriptObject_DESC		"Arariwe ComJavascriptObject Class")
set(IFBComEventSource_DESC			"Arariwe IFBComEventSource Interface")
set(AXVERSION_NUM					"1")

# NOTE: THESE GUIDS *MUST* BE UNIQUE TO YOUR PLUGIN/ACTIVEX CONTROL!  YES, ALL OF THEM!
set(FBTYPELIB_GUID				6754333e-7a53-577f-a217-535380f063a9)
set(IFBControl_GUID				ec77bd3f-bbc2-5cfc-a3cf-7607cb50a702)
set(FBControl_GUID				c48de73b-555e-5acd-a1ed-e3b49b9d22aa)
set(IFBComJavascriptObject_GUID		a782e154-2d25-5024-b06d-1ac4b2bcc692)
set(FBComJavascriptObject_GUID		361dd839-30d5-5be7-8e4d-9e23adaff23a)
set(IFBComEventSource_GUID			024ea580-b47c-5c67-8e2d-99beb64960cc)
if ( FB_PLATFORM_ARCH_32 )
	set(FBControl_WixUpgradeCode_GUID	a0ce85b3-7b01-5006-92bd-5946936496e8)
else ( FB_PLATFORM_ARCH_32 )
	set(FBControl_WixUpgradeCode_GUID	447e297a-3e5a-5554-9784-02f7ed9a2332)
endif ( FB_PLATFORM_ARCH_32 )

# these are the pieces that are relevant to using it from Javascript
set(ACTIVEX_PROGID				"arariwe.Arariwe")
if ( FB_PLATFORM_ARCH_32 )
	set(MOZILLA_PLUGINID			"arariwe.org/Arariwe")  # No 32bit postfix to maintain backward compatability.
else ( FB_PLATFORM_ARCH_32 )
	set(MOZILLA_PLUGINID			"arariwe.org/Arariwe_${FB_PLATFORM_ARCH_NAME}")
endif ( FB_PLATFORM_ARCH_32 )

# strings
set(FBSTRING_CompanyName			"The Arariwe Project")
set(FBSTRING_PluginDescription		"Extends Google Chrome, Mozilla Firefox and MS Internet Explorer cryptographic capabilities for ICP-Brasil document signing.")
set(FBSTRING_PLUGIN_VERSION			"1.0.0.0")
set(FBSTRING_LegalCopyright			"Copyright 2014 The Arariwe Project")
set(FBSTRING_PluginFileName			"np${PLUGIN_NAME}")
set(FBSTRING_ProductName			"Arariwe")
set(FBSTRING_FileExtents			"")
if ( FB_PLATFORM_ARCH_32 )
	set(FBSTRING_PluginName			"Arariwe")  # No 32bit postfix to maintain backward compatability.
else ( FB_PLATFORM_ARCH_32 )
	set(FBSTRING_PluginName			"Arariwe_${FB_PLATFORM_ARCH_NAME}")
endif ( FB_PLATFORM_ARCH_32 )
set(FBSTRING_MIMEType				"application/x-arariwe")

# Uncomment this next line if you're not planning on your plugin doing
# any drawing:

set (FB_GUI_DISABLED				0)

# Mac plugin settings. If your plugin does not draw, set these all to 0
set(FBMAC_USE_QUICKDRAW				0)
set(FBMAC_USE_CARBON				0)
set(FBMAC_USE_COCOA				0)
set(FBMAC_USE_COREGRAPHICS			0)
set(FBMAC_USE_COREANIMATION			0)
set(FBMAC_USE_INVALIDATINGCOREANIMATION	0)

# If you want to register per-machine on Windows, uncomment this line
#set (FB_ATLREG_MACHINEWIDE 1)

# Firebreath Libraries
add_firebreath_library(log4cplus)
add_firebreath_library(jsoncpp)
