/*
 * Mensagens de feed back ao usuário */
var /* String */ UNINSTALLED_PLUGIN		= "Esta página, para ser utilizada, requer a instalação prévia do plugin Arariwe. Por favor, verifique se o plugin está corretamente instalado.";
var /* String */ PLUGIN_FLAVOR		= "Esta página, para ser utilizada, requer distribuição do plugin Arariwe para o seu navegador. É possível não tenha instalado corretamente o plugin.";
var /* String */ INIT_OK			= "O plugin Arariwe está instalado e pronto para funcionar.";
var /* String */ PLUGIN_ERROR			= "Não foi possível realizar a aplicação em vista do erro _STRING_ERROR_. Consulte o log do plugin no seu diretório de usuário para informações mais detalhadas.";
var /* String */ INVALID_CONTENTS		= "É obrigatório o fornecimento de algum dado para assinar.";
var /* String */ MSG_SIGN_OK			= "Conteúdo assinado com sucesso.";
var /* String */ MSG_SIGN_FAIL		= "Ocorre um um erro ao assinar. Consulte o log do plugin.";
var /* String */ MSG_SIGN_BAD_ARGS		= "Um dos argumentos passados ao plugin não é válido. Consulte o log para maiores informações.";
var /* String */ MSG_SIGN_REFUSAL		= "O usuário, em pânico, se recusou a assinar o documento.";

/*
 * Referências globais */
var /* HTMLObjectElement */ pluginx		= null;/* Referência para o plugin */
var /* HTMLDivElement */ fbdiv		= null;/* Referência para o elemento DIV dedicado ao feedback ao usuário */
var /* Number */ PLUGIN_OK			= 0;	/* Retorno de validate_plugin() para plugin instalado corretamente */
var /* Number */ PLUGIN_NOT_INSTALLED	= 1;	/* Retorno de validate_plugin() para plugin não instalado */
var /* Number */ PLUGIN_INVALID		= 2;	/*  Retorno de validate_plugin() para plugin instalado em versão incorreta */


/* =============================================================================
 * Código de acesso à página HTML
 * ============================================================================= */
/* void */ function initPage(		/* Inicializa a página de exemplo */
    /* String */ pluginID,		/* ID da tag OBJECT utilizada para referenciar o plugin */
    /* String */ fbackID)		/* ID do elemento DIV utilizado para fornecer feedback ao usuário */
{
    fbdiv = document.getElementById(fbackID);

    /* Verifica a instalação e integridade do plugin.
     * É recomendável que esta operação seja realizada já na carga da página. */
    var /* Number */ rv = validate_plugin(pluginID);
    switch (rv)
    {
    case PLUGIN_NOT_INSTALLED:
	  givefeedback(UNINSTALLED_PLUGIN);
	  break;
    case PLUGIN_INVALID:
	  givefeedback(PLUGIN_FLAVOR);
	  break;
    default:
	  givefeedback(INIT_OK);
    }
}

/* void */ function givefeedback(	/* Fornece feedback ao usuário */
    /* String */ msg)			/* Mensagem de feedback */
{
    if (fbdiv.hasChildNodes())
    {
	    while (fbdiv.childNodes.length > 0)
	    {
		    var /* Node */ node = fbdiv.childNodes.item(0);
		    fbdiv.removeChild(node);
	    }
    }
    var /* HTMLParagraphElement */ p = document.createElement("p");
    p.innerHTML = msg;
    fbdiv.appendChild(p);
}

/* void */ function clearList(	/* Limpa o elemento SELECT especificado */
    /* HTMLSelectElement */ list)	/* O elemento HTML (obtido por document.getElementByID) */
{
    while (list.length > 0)
    {
	  list.remove(0);
    }
}

/* String */ function getSelectedItem(	/* Obtém o item selecionado do elemento SELECT especificado */
    /* String */ listID)			/* ID do elemento HTML SELECT que contém as listas */
{
    var /* String */ ret = "";
    var /* HTMLSelectElement */ select = document.getElementById(listID);
    if (select.length > 0)
    {
	    ret = select.options.item(select.selectedIndex).value;
    }
    return ret;
}

/* void */ function loadCertificates(	/* Carregas os certificados disponíveis na lista */
    /* String */ certListID,			/* ID do elemento HTML SELECT onde a lista será armazenada, para seleção */
    /* String */ mechListID)			/* ID do elemento SELECT onde a lista de algoritmos será armazenada */
{
    var /* HTMLSelectElement */ select = document.getElementById(certListID);
    clearList(select);
    var /* Array */ list = get_cert_list(givefeedback);
    for (var /* Number */ i = 0; i < list.length; i++)
    {
	  var /* Object */ certificate = list[i];
	  var /* HTMLOptionElement */ option = document.createElement("option");
	  option.setAttribute("value", certificate.Handle);
	  option.innerHTML = certificate.Subject;
	  select.appendChild(option);
    }
    verifyMechanisms(certListID, mechListID);
}

/* void */ function verifyMechanisms(	/* Exibe os algoritmos de assinatura suportados pelo certificado selecionado */
    /* String */ certListID,			/* ID do elemento SELECT onde a lista de certificados está armazenada */
    /* String */ mechListID)			/* ID do elemento SELECT onde a lista de algoritmos será armazenada */
{
    var /* String */ cert = getSelectedItem(certListID);
    var /* HTMLSelectElement */ select = document.getElementById(mechListID);
    clearList(select);
    var /* Array */ mechs = get_mechanism_list(cert, givefeedback);
    for (var /* Number */ i = 0; i < mechs.length; i++)
    {
	  var /* HTMLOptionElement */ option = document.createElement("option");
	  option.setAttribute("value", mechs[i].algorithmID);
	  option.innerHTML = mechs[i].algorithmName;
	  select.appendChild(option);
    }
}

/* void */ function signContent(	/* Assina o conteúdo especificado segundo as políticas da ICP-Brasil */
    /* String */ certListID,		/* ID do elemento SELECT onde a lista de certificados está armazenada */
    /* String */ contentTextID,	/* ID da área de texto onde o conteúdo a assinar está definido */
    /* String */ mechListID,		/* ID do elemento SELECT onde a lista de algoritmos está armazenada */
    /* String */ outputTextID)	/* ID do elemento de texto onde o documento PKCS #7 será armazenado */
{
    var /* String */ cert = getSelectedItem(certListID);
    var /* String */ mech = getSelectedItem(mechListID);
    var /* String */ contents = document.getElementById(contentTextID).value;
    if (contents && contents != "")
    {
	  sign(cert, mech, contents, signCallback, outputTextID, givefeedback);
    }
    else
    {
	  givefeedback(INVALID_CONTENTS);
    }
}

/* void */ function signFile(		/* Seleciona qualquer arquivo para assinatura segundo as políticas da ICP-Brasil */
    /* String */ certListID,		/* ID do elemento SELECT onde a lista de certificados está armazenada */
    /* String */ mechListID,		/* ID do elemento SELECT onde a lista de algoritmos está armazenada */
    /* String */ outputTextID)	/* ID do elemento de texto onde o documento PKCS #7 será armazenado */
{
    var /* String */ cert = getSelectedItem(certListID);
    var /* String */ mech = getSelectedItem(mechListID);
    signSelected(cert, mech, signCallback, outputTextID, givefeedback);
}

/* void */ function signCallback(	/* Exibe para o usuário o PKCS #7 assinado */
    /* String */ outputTextID,	/* Elemento de texto onde o documento será exibido */
    /* String */ pkcs7)			/* PKCS #7 (no formato PEM) */
{
    document.getElementById(outputTextID).value = pkcs7;
}


/* =============================================================================
 * Código de exemplo para melhor utilização do plugin Arariwe.
 * Invente por sua própria conta e risco...
 * ============================================================================= */
/* Number */ function validate_plugin(	/* Verifica a instalação e integridade do plugin */
    /* String */ pluginID)			/* ID da tag OBJECT utilizada para referenciar o plugin */
{
    var /* Number */ ret = -1;
    /* Nunca, repito, nunca verifique a string de identificação do navegador em
     * uso para validar a instalação apropriada do plugin. O Arariwe foi projetado
     * para mais de um browser e sua interface de programação é sempre consistente,
     * independente de qual o navegador é utilizado. No entanto, o usuário pode
     * instalar uma versão do plugin destinada a um browser e tentar utilizá-lo em
     * outro. Neste caso, o navegador interpretará que o plugin não está instalado.
     * É preciso, portanto testar a instalação do plugin. Simplesmente verificamos
     * se o navegador é capaz de instanciar a tag OBJECT utilizada para referenciar
     * o plugin. */
    try
    {
        pluginx = document.getElementById(pluginID);

	  /* O plugin está instalado. Agora precisamos verificar se a instalação
	   * é da versão apropriada ao browser do usuário. Fazemos isso
	   * executando o método check(), de teste de sanidade. */
	  if (pluginx)	/* O teste não é estritamente necessário. Porém, nunca se sabe... */
	  {
		try
		{
		    pluginx.check();

		    /* Se a execução foi bem sucedida, a distribuição do plugin
		     * é apropriada ao browser do usuário. */
		    ret = PLUGIN_OK;
		}
		catch (e)
		{
		    /* A falha na execução do método significa que o plugin foi
		     * incapaz de referenciar as bibliotecas criptográficas de
		     * ligação dinâmica das quais é dependente.
		     * A causa é simples: o usuário instalou uma versão do plugin
		     * para outro navegador que não o que está em uso. */
		    ret = PLUGIN_INVALID;

		    /* Em caso de falha, é mais seguro invalidar as referências ao plugin,
		     * de modo a garantir que a página não forneça erros posteriores. */
		    pluginx = null;
		}
	  }
    }
    catch (e)
    {
	  /* A tag OBJECT não foi instanciada pelo browser. Isto significa que
	   * não está registrado nele nenhum componente capaz de processar o tipo
	   * MIME application/x-xapiripe. Portanto, o plugin não está instalado */
	  ret = PLUGIN_NOT_INSTALLED;
    }
    return ret;
}

/* Array of Object */ function get_cert_list(	/* Enumera os certificados existens nos tokens presentes */
    /* Function */ callback)				/* Callback para fornecimento de feedback em caso de erro */
{
    var /* Array */ ret = new Array();
    if (pluginx)
    {
	  try
	  {
		var /* Array */ certs = pluginx.enumerateCertificates();
		for (var /* Number */ i = 0; i < certs.length; i++)
		{
		    var /* Object */ jsOb = window.JSON.parse(certs[i]);
		    /*
		     * Elementos retornados:
		     * 	jsOb.Handle: handle interno de identificação usado nas chamadas subsequentes
		     * 	jsOb.Issuer: DN do emissor
		     * 	jsOb.SerialNumber: número serial do certificado, codificado em Base64
		     * 	jsOb.Subject: DN do titular do certificado
		     * Numa seção autenticada por SSL, você pode forçar a assinatura com o certificado utilizado
		     * na seção localizando-o na lista pelas propriedades Issuer e SerialNumber. */
		    ret.push(jsOb);
		}
	  }
	  catch (e)
	  {
		callback(PLUGIN_ERROR.replace("_STRING_ERROR_", e));
	  }
    }
    return ret;
}

/* Array of Object */ function get_mechanism_list(	/* Retorna a lista de algoritmos ICP-Brasil suportados pelo certificado */
    /* Number */ hHandle,					/* Valor de CertificateHandle para o certificado selecionado */
    /* Function */ callback)					/* Callback para fornecimento de feedback em caso de erro */
{
    var /* Array */ ret = new Array();
    if (pluginx)
    {
	  try
	  {
		/* Apenas dois algoritmos são suportados nas políticas de assinatura
		 * vigentes na ICP-Brasil: SHA-1 e SHA-256. Assim, o teste é limitado
		 * a esses mecanismos. */
		if (pluginx.canSignWithHash(hHandle, pluginx.sha1))
		{
		    var /* Object */ sha1 = new Object();
		    sha1.algorithmName = "SHA-1";
		    sha1.algorithmID = pluginx.sha1;
		    ret.push(sha1);
		}
		if (pluginx.canSignWithHash(hHandle, pluginx.sha256))
		{
		    var /* Object */ sha256 = new Object();
		    sha256.algorithmName = "SHA-256";
		    sha256.algorithmID = pluginx.sha256;
		    ret.push(sha256);
		}
	  }
	  catch (e)
	  {
		callback(PLUGIN_ERROR.replace("_STRING_ERROR_", e));
	  }
    }
    return ret;
}

/* void */ function sign(		/* Assina o conteúdo com uma das políticas da ICP-Brasil */
    /* String */ certHandle,		/* Handle para o certificado a utilizar */
    /* Number */ mechanism,		/* Identificador do algoritmo de assinatura */
    /* String */ contents,		/* Conteúdo a assinar */
    /* Function */ resultCallback,	/* Callback para exibição do documento PKCS #7 */
    /* Any */ args,			/* Argumentos para resultCallback */
    /* Function */ callback)		/* Callback para fornecimento de feedback ao usuário */
{
    if (pluginx)
    {
	  /* A política de assinatura utilizada é dependente do algoritmo de hash.
	   * Consulte http://www.iti.gov.br/index.php/icp-brasil/repositorio/144-icp-brasil/repositorio/3974-artefatos-de-assinatura-digital
	   * Para SHA-1, a política é AD-RB v1.0
	   * Para SHA-256, a política é AD-RB v2.1 */
	  var /* Boolean */ v1 = (mechanism == pluginx.sha1) ? true : false;
    
	  var /* Object */ params = new Object();
	  params.digestAlgorithm = mechanism;
	  params.attachContent = true;
	  params.addSigningCertificate = true;
	  params.addSignaturePolicy = true;
	  params.sigPolicyId = v1 ? "2.16.76.1.7.1.1.1" : "2.16.76.1.7.1.1.2.1";
	  params.sigPolicyHashAlgorithm = v1 ? "1.3.14.3.2.26" : "2.16.840.1.101.3.4.2.1";
	  params.sigPolicyHashValue = v1 ? "wsACUlkGZnrx3ZFnPJFG33060cQ=" : "GyDsEDCuYeotGT1F4nL3tdsWaOY=";
	  params.sigPolicyQualifier = v1 ? "http://politicas.icpbrasil.gov.br/PA_AD_RB.der" : "http://politicas.icpbrasil.gov.br/PA_AD_RB_v2_1.der";

	  try
	  {
		/*
		 * O processo de assinatura é assíncrono (a chamada a sign retorna
		 * imediatamente). Assim precisamos de uma callback para fornecer feedback ao usuário.
		 * Utilizamos aqui uma função anônima, mas qualquer função regular em Javascript é o
		 * bastante. */
		pluginx.sign(
		    certHandle,
		    contents,
		    params,
		    function(
			  /* Number */ retCode,	/* Código de retorno. Ver documentação do plugin */
			  /* String */ pkcs7)	/* Documento assinado no formato PEM (se a operação foi bem sucedida) */
		    {
			  var /* String */ msg = "";
			  switch (retCode)
			  {
			  case pluginx.SIGN_SUCCESS:
				resultCallback(args, pkcs7);
				msg = MSG_SIGN_OK;
				break;
			  case pluginx.SIGN_FAILED:
				msg = MSG_SIGN_FAIL;
				break;
			  case pluginx.SIGN_INVALID_ARG:
				msg = MSG_SIGN_BAD_ARGS;
				break;
			  case pluginx.SIGN_REFUSAL:
				msg = MSG_SIGN_REFUSAL;
				break;
			  }
			  callback(msg);
		    }
		);
	  }
	  catch (e)
	  {
		callback(PLUGIN_ERROR.replace("_STRING_ERROR_", e));
	  }
    }
}

/* void */ function signSelected(	/* Assina o conteúdo com uma das políticas da ICP-Brasil */
    /* String */ certHandle,		/* Handle para o certificado a utilizar */
    /* Number */ mechanism,		/* Identificador do algoritmo de assinatura */
    /* Function */ resultCallback,	/* Callback para exibição do documento PKCS #7 */
    /* Any */ args,			/* Argumentos para resultCallback */
    /* Function */ callback)		/* Callback para fornecimento de feedback ao usuário */
{
    if (pluginx)
    {
	  /* A política de assinatura utilizada é dependente do algoritmo de hash.
	   * Consulte http://www.iti.gov.br/index.php/icp-brasil/repositorio/144-icp-brasil/repositorio/3974-artefatos-de-assinatura-digital
	   * Para SHA-1, a política é AD-RB v1.0
	   * Para SHA-256, a política é AD-RB v2.1 */
	  var /* Boolean */ v1 = (mechanism == pluginx.sha1) ? true : false;
    
	  var /* Object */ params = new Object();
	  params.digestAlgorithm = mechanism;
	  params.attachContent = true;
	  params.addSigningCertificate = true;
	  params.addSignaturePolicy = true;
	  params.sigPolicyId = v1 ? "2.16.76.1.7.1.1.1" : "2.16.76.1.7.1.1.2.1";
	  params.sigPolicyHashAlgorithm = v1 ? "1.3.14.3.2.26" : "2.16.840.1.101.3.4.2.1";
	  params.sigPolicyHashValue = v1 ? "wsACUlkGZnrx3ZFnPJFG33060cQ=" : "GyDsEDCuYeotGT1F4nL3tdsWaOY=";
	  params.sigPolicyQualifier = v1 ? "http://politicas.icpbrasil.gov.br/PA_AD_RB.der" : "http://politicas.icpbrasil.gov.br/PA_AD_RB_v2_1.der";

	  try
	  {
		/*
		 * O processo de assinatura é assíncrono (a chamada a sign retorna
		 * imediatamente). Assim precisamos de uma callback para fornecer feedback ao usuário.
		 * Utilizamos aqui uma função anônima, mas qualquer função regular em Javascript é o
		 * bastante. */
		pluginx.selectAndSign(
		    certHandle,
		    true,
		    params,
		    function(
			  /* Number */ retCode,	/* Código de retorno. Ver documentação do plugin */
			  /* String */ pkcs7)	/* Documento assinado no formato PEM (se a operação foi bem sucedida) */
		    {
			  var /* String */ msg = "";
			  switch (retCode)
			  {
			  case pluginx.SIGN_SUCCESS:
				resultCallback(args, pkcs7);
				msg = MSG_SIGN_OK;
				break;
			  case pluginx.SIGN_FAILED:
				msg = MSG_SIGN_FAIL;
				break;
			  case pluginx.SIGN_INVALID_ARG:
				msg = MSG_SIGN_BAD_ARGS;
				break;
			  case pluginx.SIGN_REFUSAL:
				msg = MSG_SIGN_REFUSAL;
				break;
			  }
			  callback(msg);
		    }
		);
	  }
	  catch (e)
	  {
		callback(PLUGIN_ERROR.replace("_STRING_ERROR_", e));
	  }
    }
}
